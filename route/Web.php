<?php

    use CoffeeCode\Router\Router;

    $router  = new Router('http://dev.universo.com');

    $router->namespace("App\Controllers\home");
    $router->group(null);
    $router->get("/", 'HomeController:index');

    $router->namespace("App\Controllers\login");
    $router->group(null);
    $router->get("/login", 'LoginController:index');
    $router->get("/logoff", 'LoginController:sair');
    $router->get("/cadastro", 'LoginController:cadastro');
    $router->get("/recuperar", 'LoginController:recuperarSenha');
    $router->get("/mudar-senha/{token}", 'LoginController:mundarSenha');
    $router->post("/mudar-senha", 'LoginController:mundarSenhaUpdate');
    $router->post("/esqueci-senha", 'LoginController:gerarToken');
    $router->post("/login", 'LoginController:logar');
    $router->post("/cadastro", 'LoginController:store');

    //** rotas admin **/
    $router->namespace("App\Controllers\admin");
    $router->group('admin');
    $router->get("/", 'HomeController:index');
    $router->get("/cursos", 'CursosController:index');
    $router->get("/cursos/list", 'CursosController:show');
    $router->get("/cursos/edit/{curso}", 'CursosController:edit');
    $router->post("/cursos/store", 'CursosController:store');
    $router->put("/cursos/update/{curso}", 'CursosController:update');
    $router->delete("/cursos/delete/{curso}", 'CursosController:delete');

    $router->get("/modulos", 'ModulosController:index');
    $router->get("/modulos/novo", 'ModulosController:show');
    $router->get("/modulos/edit/{modulo}", 'ModulosController:edit');
    $router->post("/modulos/store", 'ModulosController:store');
    $router->put("/modulos/update/{modulo}", 'ModulosController:update');
    $router->delete("/modulos/delete/{modulo}", 'ModulosController:delete');    

    $router->get("/aulas", 'AulasController:index');
    $router->get("/aulas/novo", 'AulasController:show');
    $router->get("/aulas/edit/{aula}", 'AulasController:edit');
    $router->post("/aulas/store", 'AulasController:store');
    $router->put("/aulas/update/{aula}", 'AulasController:update');
    $router->delete("/aulas/delete/{aula}", 'AulasController:delete');   

    //** rotas admin **/
    $router->namespace("App\Controllers\cursos");
    $router->group('cursos');
    $router->get("/", 'CursosController:index');
    $router->get("/{curso}", 'CursosController:show');
    $router->get("/{curso}/{aulaId}/{aula}", 'CursosController:showAula');
    $router->post("/aula/ajax", 'CursosController:showAulaAjax');
    $router->post("/aula/visualizada/ajax", 'CursosController:showAulaVisualizadaAjax');
    

    $router->namespace("App\Controllers");
    $router->group('oops');
    $router->get("/404", "PaginaErroController:index","erro.404");

    
    $router->dispatch();
    
    if ($router->error()) {
        
        $router->redirect("erro.404");
    }
