<?php

use App\Core\Arquivos;

$getArquivos1 = new  Arquivos;



?>
<!doctype html>
<html lang="pt-br">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="language" content="pt-br" />
    <meta name="distribution" content="Global" />
    <meta name="description" content="site de empresa de contabilidade localizada em sarandi">
    <meta name="keywords" content="escritorio contabilidade,sarandi,maringa,contabilidade,azerus contabilidade,">
    <!-- Bootstrap CSS -->
    <script
  src="https://code.jquery.com/jquery-1.12.4.js"
  integrity="sha256-Qw82+bXyGq6MydymqBxNPYTaUXXq7c8v3CwiYwLLNXU="
  crossorigin="anonymous"></script>
    <link rel="stylesheet" href="<?php $getArquivos1->access('css/bootstrap.min.css'); ?>" >
    <link rel="shortcut icon" href="<?php $getArquivos1->access('images/icon.ico'); ?>" type="image/x-icon" />
    <link href="//cdn.jsdelivr.net/npm/@sweetalert2/theme-dark@4/dark.css" rel="stylesheet">
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <title>Universo da Programação</title>
    <!-- configuraçoes de comparilhamento -->
    <meta property=" og:url" content="<?php $getArquivos1->access('images/sm-azerus-contabilidade-maringa.webp'); ?>" />
    <meta property="og:type" content="Azerus escritorio de contabilidade" />
    <meta property="og:title" content="Azerus Contabilidade" />
    <meta property="og:description" content="site de empresa de contabilidade localizada em sarandi" />
    <meta property="og:image" content="" />
  </head>
  <body>
  <style>
    .cor-site{
      background-color: #4c2c14;
    } 
    .w-75{
      width: 75%;
    }
    .cor-fundo{
      background: #f7f7f7;
    }
    .color-font-site{
      color: #512e17!important;
    }
  </style>
  <div class="cor-fundo">
    <div class="container ">
      <nav class="navbar navbar-expand-lg navbar-light ">
        <div class="logo logo-footer w-100">
       
          <div class="text-right">
        <button class="navbar-toggler text-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
          </div>
        </div>

        <div class="collapse navbar-collapse ml-5" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item active ml-3">
              <a rel="noreferrer" title="HOME" class="nav-link font-weight-bold color-font-site" href="/">HOME <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item d-none ml-3">
              <a rel="noreferrer" title="EMPRESA" class="nav-link font-weight-bold color-font-site" href="#empresa">EMPRESA</a>
            </li>

            <li class="nav-item ml-3">
              <a rel="noreferrer" title="CONTATO" class="nav-link font-weight-bold color-font-site" href="#contato">CONTATO</a>
            </li>
            <?php 
              if(isset($_SESSION['id'])){ 
            ?>
              <li class="nav-item ml-3">
                <a rel="noreferrer" title="CONTATO" class="nav-link font-weight-bold color-font-site" href="/cursos">CURSOS</a>
              </li>
            <?php } ?>

            <?php 
              if( ! isset($_SESSION['id'])){ 
            ?>
            <li class="nav-item ml-3">
              <a class="btn btn-outline-dark" href="/login" role="button">Login</a>
            </li>
            <?php } else {  ?>
            <li class="nav-item ml-3">
              <a class="btn btn-outline-dark" href="/logoff" role="button">Sair</a>
            </li>
            <?php }  ?>
          </ul>

        </div>
      </nav>
    </div>
  </div>

    <style>
        li {
      list-style-type: none;
    }
    </style>

    @yield('content')

    <footer class="  pb-3 text-center footer " style="background-color: #4c2c14;">

      <div class="container">
      <p class="text-light  pt-4">© 2020 Universo da  Programação. Todos os Direitos Reservados</p>
      </div>
    </footer>

   
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
  </body>
</html>

