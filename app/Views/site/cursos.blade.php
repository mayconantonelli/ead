@extends('layouts.app')  

@section('content')

    @php
        use App\Core\Arquivos;

        $getArquivos = new  Arquivos;
   @endphp
  
    @if(count($user->cursos))
        <div class="container mt-5  mb-5">
            <div class="row">
                <div class="col-md-12 text-center mt-5 mb-5">
                <h2 class="">Cursos</h2>
                </div>
                @foreach($user->cursos as $curso)
                    <div class="col-md-4">
                        <div class="card" style="width: 18rem;">
                            <img class="card-img-top" src="{{$getArquivos->access('images/cursos/' . $curso->imagem)}}" alt="Card image cap">
                            <div class="card-body">
                                <p class="card-text"><a href="/cursos/{{str_replace(' ','-',$curso->nome)}}">{{$curso->nome}}</a></p>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    @endif

    
    
    <div class="container mt-5  mb-5">
        <div class="row">
            <div class="col-md-12 text-center mt-5 mb-5">
            <h2 class="">Cursos Gratuitos</h2>
            </div>
            @foreach($cursosGratuitos as $cursosGratuito)
                <div class="col-md-4">
                    <div class="card" style="width: 18rem;">
                        <img class="card-img-top" src="{{$getArquivos->access('images/cursos/' . $cursosGratuito->imagem)}}" alt="Card image cap">
                        <div class="card-body">
                            <p class="card-text"><a href="/cursos/{{str_replace(' ','-',$cursosGratuito->nome)}}">{{$cursosGratuito->nome}}</a></p>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    


@endsection