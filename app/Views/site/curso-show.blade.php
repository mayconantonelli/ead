@extends('layouts.app')  
@php
   use App\Core\Arquivos;

   $getArquivos = new  Arquivos;
   @endphp
@section('content')
<div class="container-fluid">
   <div class="row">
      <div class="col-12 col-md-2 pl-0  pr-0  border">
         <div class="mb-2"></div>
         @foreach($modulos as $modulo)
            
            <div >
               <h5 class=" p-2 mb-0" >Modulo {{$loop->iteration}}: {{$modulo->nome}}</h5>
               @foreach($modulo->aulas as $aula)
                  
                  <div class="p-2 text-dark " >
                     <div class="form-group">
                        <div class="form-check ">
                              <input class="form-check-input aula-check" data-id="{{$aula->id}}" type="checkbox" id="gridCheck"
                                 @foreach($assistidos as $assistido)
                                    @if($assistido->aula_id == $aula->id)
                                       {{$assistido->assistido ? 'checked' : ''}}
                              
                                       @break
                                    @endif
                                 @endforeach
                              >
                           
                       
                           <label class="form-check-label btn-aula" data-id="{{$aula->id}}" style="cursor: pointer;">
                              <a href="#{{str_replace(' ','-',$aula->nome)}}" class="w-100 text-dark">{{$loop->iteration}}. {{$aula->nome}}</a>
                           </label>
                        </div>
                     </div>
                     
                   

                  </div>
                  
               @endforeach
            </div>
            <hr>
         @endforeach
      </div>
      <div class="col-12 col-md-10 pr-5 pl-5">
      <div class="row">
         <div class="col-md-12 mt-5 mb-5 text-center">
            <h4><span id="curso-nome"></span>  <span id="nome-aula">{{$aulaUm->nome ?? ''}}</span> </h4>
         </div>
      </div>
      <iframe 
      class="rounded mx-auto d-block"
         id="video"
         width="99%" 
         height="600" 
         src="{{$aulaUm->link ?? ''}}" 
         frameborder="0" 
         allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" 
         allowfullscreen>
      </iframe>
      </div>
   </div>
</div>
<input type="hidden" id="curso-id" value="{{$curso->id}}">
<input type="hidden" id="url" value="{{'http://'. $_SERVER['SERVER_NAME'] . '/'}}">
<input type="hidden" id="set-curso-nome" value="{{$curso->nome}}">
<style>
.html5-video-player a{
   display: none!important;
}
.btn-aula:hover {
  background-color: #f3f3f3;
}

</style>
 <script>
   $('#curso-nome').html('Cruso: ' + $('#set-curso-nome').val())

   $('.btn-aula').click(function(){
      var id = $(this).data('id')

      $.ajax({
         type: 'POST',
         url: $('#url').val() + 'cursos/aula/ajax',
         data: {
          'id': id,
         },
         success: function(response) {
            let aula = JSON.parse(response)
            $('#video').attr("src", aula.link);
            $('#nome-aula').html( aula.nome)
         }
      })

   })
   $('.aula-check').click(function(){
      var id = $(this).data('id')
      var curso_id = $('#curso-id').val()
      var checado;
      if($(this).is(':checked')){
         checado = 1;
      }else{
         checado = 0;
      }
      //console.log(checado)
 
      $.ajax({
         type: 'POST',
         url: $('#url').val() + 'cursos/aula/visualizada/ajax',
         data: {
          'aula_id': id,
          'assistido': checado,
          'curso_id':curso_id
         },
         success: function(response) {

         }
      })

   })
  
 </script>

@endsection

