<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>@yield('title')</title>
  <style>
  /****** EMAIL CLIENT BUG FIXES - BEST NOT TO CHANGE THESE ********/
  
  .ExternalClass {
    width: 100%;
  }
  /* Forces Outlook.com to display emails at full width */
  .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div { line-height: 100%; }
  /* Forces Outlook.com to display normal line spacing, here is more on that: http://www.emailonacid.com/forum/viewthread/43/ */
  
 /* Normalize.css */
/* Contents of this file must be inlined to your newsletter in production */

h1 a,
h2 a,
h3 a,
h4 a,
h5 a,
h6 a,
li a,
p a {
  /* Set sexy underline styling for links except images */
  text-decoration: none;
  color: #2837b8 !important;
  border-bottom: #d3d6f0 1px solid;
}
h1 {
  /* Mail.ru <h1> styling fix */
  font-size: 2em;
  line-height: initial;
  margin: 0.2em 0;
  padding: 0;
},
p {
  margin-top: 0.2rem;
  margin-bottom: 0.2rem;
}
table {
  /* Null tables spaces */
  border-spacing: 0;
  border-collapse: collapse;
}
table td {
  padding: 0;
}
table th {
  padding: 0;
  font-weight: normal;
}
img {
  /* Flexible images fix + prevent any borders for images */
  max-width: 100%;
  border: 0;
  outline: 0;
  /* Set image's ALT text styling */
  color: #2837b8;
  font-size: 14px;
}
ol,
ul {
  /* We don't touch horizontal margins to prevent hiding bullets in Oultook */
  margin-top: 1em;
  margin-bottom: 2em;
}
ol li,
ul li {
  line-height: 1.6em;
  margin: 0.5em 0;
}
p {
  line-height: 1.6em;
  margin: 1em 0;
}
span.code {
  /* Monospace emphasis for code examples */
  font-family: Arial, Helvetica, sans-serif;
  color: grey;
}
    .wrap {
      max-width: 791px;
      text-align: center;
      margin-left: auto;
      margin-right: auto;
      border: 2px solid #004B88;
    }
    .header img {
      margin-left: auto;
      margin-right: auto;
      width: 100%;
    }
    .content {
      padding: 1.2rem;
    }
    .btn {
      padding: .8rem 1rem;
      border-radius: 5px;
      border: 1px solid #eee;
      font-weight: bold;
      text-decoration: none;
    }
    .btn-success {
      background-color: #28a745;
      border-color: #28a745;
      color: white;
    }
    .btn-success:hover {
      background-color: #218838;
      border-color: #1e7e34;
    }
  </style>
</head>
<body>
  <div class="wrap">
    <div class="header">
      <img title=""  alt="">
    </div>
    <div class="content">
      @yield
    </div>
  </div>
</body>
</html>