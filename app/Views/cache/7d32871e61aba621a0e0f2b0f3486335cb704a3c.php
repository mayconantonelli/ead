  

<?php $__env->startSection('content'); ?>

    <?php
        use App\Core\Arquivos;

        $getArquivos = new  Arquivos;
   ?>
  
    <?php if(count($user->cursos)): ?>
        <div class="container mt-5  mb-5">
            <div class="row">
                <div class="col-md-12 text-center mt-5 mb-5">
                <h2 class="">Cursos</h2>
                </div>
                <?php $__currentLoopData = $user->cursos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $curso): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="col-md-4">
                        <div class="card" style="width: 18rem;">
                            <img class="card-img-top" src="<?php echo e($getArquivos->accessImg($curso->imagem)); ?>" alt="Card image cap">
                            <div class="card-body">
                                <p class="card-text"><a href="/cursos/<?php echo e($curso->nome); ?>"><?php echo e($curso->nome); ?></a></p>
                            </div>
                        </div>
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
        </div>
    <?php endif; ?>

    
    
    <div class="container mt-5  mb-5">
        <div class="row">
            <div class="col-md-12 text-center mt-5 mb-5">
            <h2 class="">Cursos Gratuitos</h2>
            </div>
            <?php $__currentLoopData = $cursosGratuitos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cursosGratuito): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="col-md-4">
                    <div class="card" style="width: 18rem;">
                        <img class="card-img-top" src="<?php echo e($getArquivos->accessImg($cursosGratuito->imagem)); ?>" alt="Card image cap">
                        <div class="card-body">
                            <p class="card-text"><a href="/cursos/<?php echo e($cursosGratuito->nome); ?>"><?php echo e($cursosGratuito->nome); ?></a></p>
                        </div>
                    </div>
                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </div>
    


<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\sistemas\universo\app\Views/site/cursos.blade.php ENDPATH**/ ?>