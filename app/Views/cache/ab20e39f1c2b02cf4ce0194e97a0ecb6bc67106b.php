<?php

use App\Core\Arquivos;

$getArquivos1 = new  Arquivos;



?>
<!doctype html>
<html lang="pt-br">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="language" content="pt-br" />
    <meta name="distribution" content="Global" />
    <meta name="description" content="site de empresa de contabilidade localizada em sarandi">
    <meta name="keywords" content="escritorio contabilidade,sarandi,maringa,contabilidade,azerus contabilidade,">
    <!-- Bootstrap CSS -->
    
    <link rel="stylesheet" href="<?php $getArquivos1->access('css/bootstrap.min.css'); ?>" >
    <link rel="shortcut icon" href="<?php $getArquivos1->access('images/icon.ico'); ?>" type="image/x-icon" />
    
    <title>Azerus Contabilidade</title>
    <!-- configuraçoes de comparilhamento -->
    <meta property=" og:url" content="<?php $getArquivos1->access('images/sm-azerus-contabilidade-maringa.webp'); ?>" />
    <meta property="og:type" content="Azerus escritorio de contabilidade" />
    <meta property="og:title" content="Azerus Contabilidade" />
    <meta property="og:description" content="site de empresa de contabilidade localizada em sarandi" />
    <meta property="og:image" content="" />
  </head>
  <body>
  <style>
    .cor-site{
      background-color: #4c2c14;
    } 
    .w-75{
      width: 75%;
    }
    .cor-fundo{
      background: #f7f7f7;
    }
    .color-font-site{
      color: #512e17!important;
    }
  </style>
  <div class="cor-fundo">
    <div class="container ">
      <nav class="navbar navbar-expand-lg navbar-light ">
        <div class="logo logo-footer">
          <img class="w-75" src="<?php $getArquivos1->access('images/sm-azerus-contabilidade-maringa.webp'); ?>" alt="" title="" >
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        </div>

        <div class="collapse navbar-collapse ml-5" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item active ml-3">
              <a rel="noreferrer" title="HOME" class="nav-link font-weight-bold color-font-site" href="/">HOME <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item d-none ml-3">
              <a rel="noreferrer" title="EMPRESA" class="nav-link font-weight-bold color-font-site" href="#empresa">EMPRESA</a>
            </li>
            <li class="nav-item ml-3">
              <a rel="noreferrer" title="SERVIÇOS" class="nav-link font-weight-bold color-font-site" href="#servicos">SERVIÇOS</a>
            </li>
            <li class="nav-item ml-3">
              <a rel="noreferrer" title="CONTATO" class="nav-link font-weight-bold color-font-site" href="#contato">CONTATO</a>
            </li>


          </ul>

        </div>
      </nav>
    </div>
  </div>

    <style>
        li {
      list-style-type: none;
    }
    </style>

    <?php echo $__env->yieldContent('content'); ?>

    <footer class="  pb-3 text-center footer " style="background-color: #4c2c14;">
      <div class="pt-3 pb-3" style="background: #f7f7f7;">
        <div class="container" >
          <div class=" row">
            <div class="col-md-6">
              <div class="logo logo-footer">
                <img src="<?php $getArquivos1->access('images/sm-azerus-contabilidade-maringa.webp'); ?>" alt="" title="" >
              </div>
              <ul class="social-links">
                  <li>
                    <a title="instagram" rel="noreferrer" href="https://www.instagram.com/epsmaringa/" target="_blank">
                      <i class="fab fa-instagram"></i>
                    </a>
                  </li>
                  
                  <li>
                    <a title="facebook" rel="noreferrer" href="https://www.facebook.com/epsdrywalleisopor/" target="_blank">
                      <i class="fab fa-facebook-f"></i>
                    </a>
                  </li>
              </ul> 
            </div>
            <div class="col-md-6" id="contato">
              <h2 >CONTATO</h2>
              <ul class="list clearfix text-font ">
                  <li >Rua aglacir binda albert,56 - Sarandi/PR</li>
                  <li>CEP: 87113-460</li>
                  <li><a href="https://api.whatsapp.com/send?phone=554499846-0971">44 99846-0971 Whatsapp</a> </li>
                  <li>contato@azeruscontabilidade.com</li>                                           
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="container">
      <p class="text-light mt-3 pt-3">© 2020 Azerus Contabilidade - Sarandi. Todos os Direitos Reservados | Desenvolvimento  by Agência Kairos </p>
      </div>
    </footer>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
  </body>
</html><?php /**PATH C:\xampp\htdocs\sites\azerucontabilidade\app\Views/layouts/app.blade.php ENDPATH**/ ?>