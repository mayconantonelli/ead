  
<?php
   use App\Core\Arquivos;

   $getArquivos = new  Arquivos;
   ?>
<?php $__env->startSection('content'); ?>
<div class="container-fluid">
   <div class="row">
      <div class="col-12 col-md-2 pl-0  pr-0  border">
         <div class="mb-2"></div>
         <?php $__currentLoopData = $modulos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $modulo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            
            <div >
               <h5 class=" p-2 mb-0" >Modulo <?php echo e($loop->iteration); ?>: <?php echo e($modulo->nome); ?></h5>
               <?php $__currentLoopData = $modulo->aulas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $aula): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  
                  <div class="p-2 text-dark " >
                     <div class="form-group">
                        <div class="form-check ">
                              <input class="form-check-input aula-check" data-id="<?php echo e($aula->id); ?>" type="checkbox" id="gridCheck"
                                 <?php $__currentLoopData = $assistidos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $assistido): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php if($assistido->aula_id == $aula->id): ?>
                                       <?php echo e($assistido->assistido ? 'checked' : ''); ?>

                              
                                       <?php break; ?>
                                    <?php endif; ?>
                                 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                              >
                           
                       
                           <label class="form-check-label btn-aula" data-id="<?php echo e($aula->id); ?>" style="cursor: pointer;">
                              <a href="#<?php echo e(str_replace(' ','-',$aula->nome)); ?>" class="w-100 text-dark"><?php echo e($loop->iteration); ?>. <?php echo e($aula->nome); ?></a>
                           </label>
                        </div>
                     </div>
                     
                   

                  </div>
                  
               <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
            <hr>
         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
      </div>
      <div class="col-12 col-md-10 pr-5 pl-5">
      <div class="row">
         <div class="col-md-12 mt-5 mb-5 text-center">
            <h4><span id="curso-nome"></span>  <span id="nome-aula"><?php echo e($aulaUm->nome ?? ''); ?></span> </h4>
         </div>
      </div>
      <iframe 
      class="rounded mx-auto d-block"
         id="video"
         width="99%" 
         height="600" 
         src="<?php echo e($aulaUm->link ?? ''); ?>" 
         frameborder="0" 
         allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" 
         allowfullscreen>
      </iframe>
      </div>
   </div>
</div>
<input type="hidden" id="curso-id" value="<?php echo e($curso->id); ?>">
<input type="hidden" id="set-curso-nome" value="<?php echo e($curso->nome); ?>">
<style>
.html5-video-player a{
   display: none!important;
}
.btn-aula:hover {
  background-color: #f3f3f3;
}

</style>
 <script>
   $('#curso-nome').html('Cruso: ' + $('#set-curso-nome').val())

   $('.btn-aula').click(function(){
      var id = $(this).data('id')

      $.ajax({
         type: 'POST',
         url: 'http://dev.universo.com/cursos/aula/ajax',
         data: {
          'id': id,
         },
         success: function(response) {
            let aula = JSON.parse(response)
            $('#video').attr("src", aula.link);
            $('#nome-aula').html( aula.nome)
         }
      })

   })
   $('.aula-check').click(function(){
      var id = $(this).data('id')
      var curso_id = $('#curso-id').val()
      var checado;
      if($(this).is(':checked')){
         checado = 1;
      }else{
         checado = 0;
      }
      //console.log(checado)
 
      $.ajax({
         type: 'POST',
         url: 'http://dev.universo.com/cursos/aula/visualizada/ajax',
         data: {
          'aula_id': id,
          'assistido': checado,
          'curso_id':curso_id
         },
         success: function(response) {

         }
      })

   })
  
 </script>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\sistemas\ead\app\Views/site/curso-show.blade.php ENDPATH**/ ?>