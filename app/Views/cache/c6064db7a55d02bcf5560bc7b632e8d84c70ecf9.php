  

<?php $__env->startSection('content'); ?>

    <?php
        use App\Core\Arquivos;

        $getArquivos = new  Arquivos;
   ?>
   <div  >

      <div class="container mt-5  mb-5">
         <div class="row">
            <div class="col-md-12  mt-5 mb-5">
            <h4 class="text-center mb-3">insira sua nova senha</h4>
            <form method="POST" action="/mudar-senha" data-parsley-validate>
                <input type="hidden" value="POST" name="_method">
                    <div class="form-group">
                        <label for="email">Endereço de email</label>
                        <input type="email" class="form-control" id="email" aria-describedby="email" name="email" required placeholder="Seu email">
                        <input type="hidden" value="<?php echo e($token); ?>" name="token">
                        <input type="password" id="senha" class="form-control mt-2" name="senha" placeholder="Senha" required>
                    </div>

                    <button type="submit" class="btn btn-primary">enviar</button>
                    </form>
                </div>
          

         </div>
      </div>
   </div>
<style>
.parsley-required{
    color: red;
}
</style>
<script src="<?php echo e($getArquivos->access('js/parsley.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\sistemas\ead\app\Views/site/recuperar-senha.blade.php ENDPATH**/ ?>