   <?php

      use App\Core\Arquivos;

      $getArquivos = new  Arquivos;
      
      $getArquivos->view('head');


   ?>
   <div class="capa">
      <div  class=" text-center holder" >

         
         <img src="<?php $getArquivos->access('images/banner-2.webp'); ?>" class=" img-fluid holder2" alt="Imagem responsiva" style="
    opacity: 0.4;
">
         <h1 class=" titulo-principal holder2 h2">Entre em contato conosco!</h1>

      </div>
   </div>

<script>
function typeWrite(elemento){
  const textoArray = elemento.innerHTML.split('');
  elemento.innerHTML = ' ';
  textoArray.forEach(function(letra, i){   
     
  setTimeout(function(){
     elemento.innerHTML += letra;
  }, 75 * i)

});
}
const titulo = document.querySelector('.titulo-principal');
typeWrite(titulo);

</script>

<style>
  .titulo-principal{
        
        text-align: center;
        margin: 60px auto;
        font-family:'Courier New', Courier, monospace;
        color: #fff;
        position: absolute;
   z-index: 999;
        font-family: sans-serif;
        text-align: center;
    }

  .titulo-principal:after{
    content: '|';
    margin-left: 5px;
    opacity: 1;
    animation: pisca .7s infinite;
    z-index: 999;
    }
 
    @keyframes  pisca{
        0%, 100%{
            opacity: 1;
        }
        50%{
            opacity: 0;
        }
    }
    .holder {
        display: flex;
        justify-content: center;
        align-items: center;
        background: black;
        z-index: 99;
    }
    .holder2 {
        
      
        font-family: sans-serif;
        text-align: center;
    }
</style>

   <div class="container mt-5  " id="servicos">
         <div class="row">
            <div class="col-md-12 text-center  mb-5">
               <h2 class="">Serviços</h2>
            </div>
            <div class="col-md-4">
               <h3><i class="bi bi-bookmark-star-fill"></i>Legalização</h3>
               <p>  Abertura e encerramento de empresas; Alterações contratuais e acompanhamento dos trâmites legais perante os órgãos Federal, Estadual e Municipal.</p>
            </div>
            <div class="col-md-4">
               <h3><i class="bi bi-calculator-fill"></i>Contábil</h3>
               <p>  Escrituração contábil de acordo com as normas vigentes; Elaboração e análise das demonstrações contábeis e assessoria contábil periódica. </p>
            </div>
            <div class="col-md-4 mb-5">
               <h3><i class="bi bi-server"></i>Fiscal</h3>
               <p>  Escrituração dos registros fiscais; Apuração de tributos; Efetivação de parcelamentos; Entrega das obrigações acessórias e acompanhamento a eventuais fiscalizações. </p>
            </div>
            <div class="col-md-4 mb-5">
               <h3><i class="bi bi-people-fill"></i>Pessoal</h3>
               <p> Processo admissional e demissional; Elaboração de folha de pagamento, Férias e 13º salário; Emissão de guias trabalhistas; Rescisões e férias; Entrega de declarações; Acompanhamento em processos de fiscalização; Assessoria na aplicação de legislação trabalhista e previdenciária. </p>
            </div>
            <div class="col-md-4 mb-5">
               <h4><i class="bi bi-journal-check"></i>Consultoria Fiscal e Tributária</h3>
               <p> Análise e avaliação de procedimentos fiscais; Revisão de tributos diretos e indiretos; Aproveitamento de incentivos fiscais; Planejamento tributário; Preparação e revisão de declarações acessórias e assessoria fiscal periódica. </p>
            </div>
            <div class="col-md-4 mb-5">
              
               <h3> <i class="bi bi-person-fill"></i>Pessoa física</h3>
               <p>Assessoria para declarações de Imposto de Renda Pessoa, Carnê-Leão, Geração de Documento de Arrecadação do eSocial – DAE.</p>
            </div>
         </div>
      </div>
   <div class="container d-none" >
      <div class="row">
         <div class="col-md-12  mt-5">
            <h2 class="text-center">Empresa</h2>
            <p class="mt-4 mb-4">sadasdasdasdd</p>
         </div>
         <div class="col-md-4">
            <h3>serviço 1</h3>
            <p>  texto texto texto texto    texto texto texto texto   texto texto texto texto </p>
         </div>
         <div class="col-md-4">
            <h3>serviço 2</h3>
            <p>  texto texto texto texto    texto texto texto texto   texto texto texto texto </p>
         </div>
         <div class="col-md-4 mb-5">
            <h3>serviço 3</h3>
            <p>  texto texto texto texto    texto texto texto texto   texto texto texto texto </p>
         </div>
      </div>
   </div>






<?php  $getArquivos->view('footer'); ?><?php /**PATH C:\xampp\htdocs\sites\azerucontabilidade\app\Views/home/index.blade.php ENDPATH**/ ?>