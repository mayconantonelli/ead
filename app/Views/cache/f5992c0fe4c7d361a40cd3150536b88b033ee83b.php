  

<?php $__env->startSection('content'); ?>

    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Modulos</h1>

    </div>
    <div class="card shadow mb-4">
        <div class="card-body">
            <div class="table-responsive">
                <div id="dataTable_wrapper" class="dataTables_wrapper dt-bootstrap4">
                    <div class="text-right">

                        <a href="/admin/modulos/novo" class="btn btn-primary btn-icon-split mb-3">
                            <span class="icon text-white-50">
                                <i class="fas fa-plus"></i>
                            </span>
                            <span class="text">Adicionar Modulos</span>
                        </a>
                    </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <table class="table table-bordered dataTable" id="dataTable" width="100%" cellspacing="0" role="grid" aria-describedby="dataTable_info" style="width: 100%;">
                                    <thead>
                                        <tr role="row">
                                            <th class="sorting_asc" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 65px;">
                                                Nome
                                            </th>
                                
                    
                                            <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-label="Start date: activate to sort column ascending" style="width: 68px;">
                                                Curso
                                            </th>
                                            <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-label="Start date: activate to sort column ascending" style="width: 68px;">
                                                Opções
                                            </th>
                                        </tr>
                                    </thead>
        

                                    <tbody>
                                        <?php $__currentLoopData = $modulos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $modulo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr role="row" class="odd">
                                                <td class="sorting_1"><?php echo e($modulo->nome); ?></td>
                                                <td>
                                                <?php $__currentLoopData = $cursos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $curso): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <?php if($curso->id == $modulo->curso_id): ?>
                                                        <?php echo e($curso->nome); ?>

                                                    <?php endif; ?>
                                                    

                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </td>
                                                <td>
                                                
                                                        <form class="p-0" method="POST" action="<?php echo e($getArquivos->route('admin/modulos/delete/' . $modulo->id)); ?>">
                                                            <a href="<?php echo e($getArquivos->route('admin/modulos/edit/' . $modulo->id)); ?>" class="btn btn-success btn-circle ">
                                                                <i class="fas fa-edit"></i>
                                                            </a>
                                                            <input type="hidden" name="_method" value="DELETE"/>
                                                            <button class="btn btn-danger">
                                                        
                                                                <i class="fas fa-trash"></i>
                                                        
                                                            </button>
                                                        </form>
                                                   
                                                </td>
                                            </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\sistemas\ead\app\Views/admin/modulos/index.blade.php ENDPATH**/ ?>