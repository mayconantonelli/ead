  

<?php $__env->startSection('content'); ?>
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Adicionar Curso</h1>

    </div>
    
    <div class="container">
        <div class="row">
            <form method="post" action="<?php echo e($route); ?>" enctype="multipart/form-data">
                 <?php if(isset($curso)): ?>
                 <input type="hidden" value="PUT" name="_method">
                <?php else: ?>
                <input type="hidden" value="POST" name="_method">
                <?php endif; ?>
                <div class="form-group">
                    <label for="nome">Nome</label>
                    <input type="text" class="form-control" id="nome" name="nome" value="<?php echo e(isset($curso) ? $curso->nome : ''); ?>" placeholder="nome" required>
                </div>


                <div class="form-group">
                    <label for="pago">Pago</label>
                    <select class="form-control" id="pago" name="pago">
                        <option value="1" 
                        <?php echo e(isset($curso) && $curso->pago  == 1 ? 'selected' : ''); ?>>
                        Sim
                        </option>

                        <option value="0"
                         <?php echo e(isset($curso) && $curso->pago  == 0 ? 'selected' : ''); ?>

                         >Não</option>
                    </select>
                </div>   
                <div class="form-group">
                    <label for="imagem">Imagem</label>
                    <input type="file" class="form-control-file" id="imagem" name="imagem" <?php echo e(isset($curso) ? '' : 'required'); ?>>
                </div>
                <?php if(isset($curso)): ?>             
                    <div class="form-group mb-4">
          
                        <img width="100" src="<?php echo e($getArquivos->access('images/cursos/' . $curso->imagem)); ?>" alt="">
                    </div>
                <?php endif; ?>
                <button class="btn btn-success btn-icon-split">
                <span class="btn btn-success btn-icon-split">
                    <span class="icon text-white-50">
                        <i class="fas fa-check"></i>
                    </span>
                        <span class="p-2">Salvar</span>
                    </span>
                </button>
                
            </form>
        </div>
    </div>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\sistemas\ead\app\Views/admin/cursos/show.blade.php ENDPATH**/ ?>