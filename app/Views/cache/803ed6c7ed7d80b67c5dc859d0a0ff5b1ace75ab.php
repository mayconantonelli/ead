  

<?php $__env->startSection('content'); ?>
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Adicionar Modulos</h1>

    </div>
    
    <div class="container">
        <div class="row">
            <form method="post" action="<?php echo e($route); ?>" enctype="multipart/form-data">
                 <?php if(isset($modulo)): ?>
                 <input type="hidden" value="PUT" name="_method">
                <?php else: ?>
                <input type="hidden" value="POST" name="_method">
                <?php endif; ?>



                <div class="form-group">
                    <label for="curso_id">Curso</label>
                    <select class="form-control" id="curso_id" name="curso_id">
                        <?php $__currentLoopData = $cursos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $curso): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($curso->id); ?>" 
                            <?php echo e(isset($modulo->id) && $curso->id  == $modulo->curso_id ? 'selected' : ''); ?>>
                            <?php echo e($curso->nome); ?>

                            </option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


                    </select>
                </div>   
                <div class="form-group">
                    <label for="nome">Nome do Modulo</label>
                    <input type="text" class="form-control" id="nome" name="nome" value="<?php echo e(isset($modulo) ? $modulo->nome : ''); ?>" placeholder="nome" required>
                </div>
 
                <button class="btn btn-success btn-icon-split">
                <span class="btn btn-success btn-icon-split">
                    <span class="icon text-white-50">
                        <i class="fas fa-check"></i>
                    </span>
                        <span class="p-2">Salvar</span>
                    </span>
                </button>
                
            </form>
        </div>
    </div>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\sistemas\ead\app\Views/admin/modulos/show.blade.php ENDPATH**/ ?>