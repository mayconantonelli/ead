@extends('layouts.admin')  

@section('content')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Adicionar Aula</h1>

    </div>
    
    <div class="container">
        <div class="row">
            <form method="post" action="{{$route}}" enctype="multipart/form-data">
                 @if(isset($aula))
                 <input type="hidden" value="PUT" name="_method">
                @else
                <input type="hidden" value="POST" name="_method">
                @endif



                <div class="form-group">
                    <label for="curso_id">Curso</label>
                    <select class="form-control" id="curso_id" name="curso_id">
                        @foreach($cursos as $curso)
                            <option value="{{$curso->id}}" 
                            {{isset($aula->id) && $curso->id  == $aula->curso_id ? 'selected' : ''}}>
                            {{$curso->nome}}
                            </option>
                        @endforeach


                    </select>
                </div>   
                <div class="form-group">
                    <label for="curso_id">Modulo</label>
                    <select class="form-control" id="curso_id" name="modulo_id">
                        @foreach($modulos as $modulo)
                            <option value="{{$modulo->id}}" 
                            {{isset($aula->id) && $modulo->id  == $aula->modulo_id ? 'selected' : ''}}>
                            {{$curso->nome}}
                            </option>
                        @endforeach


                    </select>
                </div>   
                <div class="form-group">
                    <label for="nome">Nome do aula</label>
                    <input type="text" class="form-control" id="nome" name="nome" value="{{isset($aula) ? $aula->nome : ''}}" placeholder="nome" required>
                </div>
 
                <button class="btn btn-success btn-icon-split">
                <span class="btn btn-success btn-icon-split">
                    <span class="icon text-white-50">
                        <i class="fas fa-check"></i>
                    </span>
                        <span class="p-2">Salvar</span>
                    </span>
                </button>
                
            </form>
        </div>
    </div>


@endsection
