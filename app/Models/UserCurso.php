<?php

    namespace App\Models;

    use Illuminate\Database\Eloquent\Model;

    class UserCurso extends Model
    {
        protected $table = 'usercursos';

        protected $fillable = [
            "user_id",
            "curso_id",
        ];
    }