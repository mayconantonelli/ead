<?php

    namespace App\Models;

    use Illuminate\Database\Eloquent\Model;

    class Aula extends Model
    {
        public $timestamps = false;
        protected $fillable = [
            "nome",
            "modulo_id",
            "ordem",
            "curso_id"
        ];
        public function aulasAssistidas(){
            return $this->hasMany(AulaAssistida::class);
        }
        
    }