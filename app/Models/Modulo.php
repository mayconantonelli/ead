<?php

    namespace App\Models;

    use Illuminate\Database\Eloquent\Model;

    class Modulo extends Model
    {
        public $timestamps = false;
        protected $fillable = [
            "nome",
            "curso_id",
        ];

        public function aulas(){
            return $this->hasMany(Aula::class);
        }

    }