<?php

    namespace App\Models;

    use Illuminate\Database\Eloquent\Model;

    class AulaAssistida extends Model
    {
        protected $table = 'aula_assistida';
        public $timestamps = false;
        protected $fillable = [
            "aula_id",
            "user_id",
            "assistido",
            "curso_id"
        ];
    }