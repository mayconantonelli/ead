<?php

    namespace App\Models;

    use Illuminate\Database\Eloquent\Model;

    class User extends Model
    {
        protected $fillable = [
            "nome",
            "email",
            "senha",
            "admin"
        ];


        public function cursos(){
            return $this->belongsToMany('App\Models\Curso','usercursos');
        }
    }