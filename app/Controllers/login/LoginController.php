<?php

    namespace App\Controllers\login;

use App\Core\Arquivos;
use App\Core\Emails;
use App\Models\User;


class LoginController extends Arquivos
    {
        public function index()
        {
            if(isset($_SESSION['id'])){
                $this->redirect('cursos');
            }
            return $this->view('site.login');
        }
        public function cadastro()
        {
            if(isset($_SESSION['id'])){
                $this->redirect('cursos');
            }
            return $this->view('site.cadastro');
        }
        public function logar($data)
        {
            $email = str_replace(" ", "", $data['email']);
            $senha = str_replace(" ", "", $data['senha']);

            /*if(strlen($senha) < 6 ){
                echo '<script>alert("senha tem que ter no minimo 6 caracteres")</script>';
                $this->redirect('login');
            }*/
            if( ! filter_var($email, FILTER_VALIDATE_EMAIL)){
                echo '<script>alert("email invalido!")</script>';
                return $this->redirect('login');
            }
            $_SESSION['email'] = $email;
            $user = User::where('email',$email)->first();

            if( ! $user){
                $_SESSION['error'] = "Usuario ou senha estão errados!";
                
                
                return $this->redirect('login');
            }

            if( ! password_verify($senha,$user->senha)){
                $_SESSION['error'] = "Usuario ou senha estão errados!";
                return $this->redirect('login');
            }

            if($user){
                $_SESSION['email'] = $user->email;
                $_SESSION['id'] = $user->id;

                
                if(isset($_SESSION['error'])){
                   unset($_SESSION['error']); 
                }

                return $this->redirect('cursos');
            }

        }
        public function store($data)
        {
            
            $email = str_replace(" ", "", $data['email']);
            $senha = str_replace(" ", "", $data['senha']);
            $nome = str_replace(" ", "", $data['nome']);

            
            if(strlen($senha) < 6 ){
                echo '<script>alert("senha tem que ter no minimo 6 caracteres")</script>';
                 return $this->redirect('cadastro');
            }
            if($email === ''){
                echo '<script>alert("email não pode estar vazio!")</script>';
                return $this->redirect('cadastro');
            }
            if( ! filter_var($email, FILTER_VALIDATE_EMAIL)){
                echo '<script>alert("email invalido!")</script>';
                return $this->redirect('cadastro');
            }

            $user = User::where('email',$email)->first();

            if($user){
                echo '<script>alert("Usuario ja esta cadastrado!")</script>';
                return $this->redirect('cadastro');
            }

            $user = new User;

            $user->nome = $nome;
            $user->email = $email;
            $user->senha = password_hash($senha, PASSWORD_DEFAULT);

            $user->save();

    

            if($user){
                $_SESSION['email'] = $user->email;
                $_SESSION['id'] = $user->id;
                echo '<script>alert("cadastro realizado com sucesso!")</script>';
                return $this->redirect('cursos');
            }

        }
        public function sair()
        {
            unset($_SESSION['email']);
            unset($_SESSION['id']);
            return $this->redirect('login');
        }
        public function gerarToken($data){
            $user = User::where('email',$data['email'])->first();

            if($user){
                $email = new Emails();
                $token = md5($user->email . $user->senha . date("d:h"));
                
                $url = 'http://'. $_SERVER['SERVER_NAME'] . '/mudar-senha/' .  $token;

                $mensagem = "<a href='" . $url  . "'>link para recuperar senha </a>";

                $email->send($user->email,'recuperação senha universo da programação',$mensagem); 

                echo '<script>alert("Foi enviado para seu email link para recuperar senha!")</script>';
                return $this->redirect('recuperar');
            }

            echo '<script>alert("email não encontrado!")</script>';

            return $this->redirect('recuperar');
            
        }

        public function recuperarSenha()
        {
            return $this->view('site.esqueci-senha');
        }

        public function mundarSenha($data)
        {
            $token = $data['token'];
            return $this->view('site.recuperar-senha',compact('token'));
        }

        public function mundarSenhaUpdate($data)
        {
            $user = User::where('email',$data['email'])->first();

            $token = md5($user->email . $user->senha . date("d:h"));

            if($token == $data['token']){
                $user->senha = password_hash($data['senha'], PASSWORD_DEFAULT);
                $user->save();
                echo '<script>alert("senha alterada com sucesso!")</script>';
                return $this->redirect('login');
            }
            
            echo '<script>alert("token errado!")</script>';
            return $this->redirect('login');
        }
        
    }