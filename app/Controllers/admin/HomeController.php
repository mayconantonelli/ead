<?php

    namespace App\Controllers\admin;

    use App\Core\Arquivos;
use App\Models\User;

class HomeController extends Arquivos
    {
        public function index(){
            if( ! isset($_SESSION['id'])){
                return $this->redirect('login');
            }

            $user = User::where('id',$_SESSION['id'])->first();

            if( ! $user->admin){
               
                return $this->redirect('login');
            }

            
            
            return $this->view('admin.index');
        }

    }