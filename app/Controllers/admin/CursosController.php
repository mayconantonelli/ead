<?php

    namespace App\Controllers\admin;

    use App\Core\Arquivos;
use App\Core\Certificado;
use App\Core\Image;
use App\Models\Curso;
    use App\Models\User;


use Exception;

class CursosController extends Arquivos
    {
        public function index(){
            if( ! isset($_SESSION['id'])){
                return $this->redirect('login');
            }

            $user = User::where('id',$_SESSION['id'])->first();

            if( ! $user->admin){
               
                return $this->redirect('login');
            }
          

            $getArquivos = new  Arquivos;


            $cursos = Curso::all();
            
            
            return $this->view('admin.cursos.index',compact('cursos','getArquivos'));
        }
        public function show(){
            if( ! isset($_SESSION['id'])){
                return $this->redirect('login');
            }

            $user = User::where('id',$_SESSION['id'])->first();

            if( ! $user->admin){
               
                return $this->redirect('login');
            }

            
            
            $route = $this->route('admin/cursos/store');
            
            
            return $this->view('admin.cursos.show',compact('route'));
        }
        public function store($data){
            if( ! isset($_SESSION['id'])){
                return $this->redirect('login');
            }

            $user = User::where('id',$_SESSION['id'])->first();

            if( ! $user->admin){
               
                return $this->redirect('login');
            }

            $curso = new Curso();

            if ($_FILES) {
                $extensao = pathinfo($_FILES['imagem']['name'],PATHINFO_EXTENSION);
                $pasta = 'public/images/cursos/';
                $temporario = $_FILES['imagem']['tmp_name'];
                $nome = str_replace(' ','-',$data['nome']) . '-' . uniqid(). '.' . $extensao;

                move_uploaded_file($temporario,$pasta.$nome);
                    
            }

            $curso->nome = $data['nome'];
            $curso->pago = $data['pago'];
            $curso->imagem = $nome;
            $curso->save();
            
            return $this->redirect('admin/cursos');
        }
        public function edit($data){
            if( ! isset($_SESSION['id'])){
                return $this->redirect('login');
            }

            $user = User::where('id',$_SESSION['id'])->first();

            if( ! $user->admin){
               
                return $this->redirect('login');
            }
            $curso = Curso::find($data['curso']);

            $route = $this->route('admin/cursos/update/' . $curso->id);
           
            $getArquivos = new  Arquivos;
            
            return $this->view('admin.cursos.show',compact('curso','getArquivos','route'));
        }
        public function delete($dado){
            if( ! isset($_SESSION['id'])){
                return $this->redirect('login');
            }

            $user = User::where('id',$_SESSION['id'])->first();

            if( ! $user->admin){
               
                return $this->redirect('login');
            }
            if ($curso = Curso::find($dado['curso'])) {
                unlink('public/images/cursos/' . $curso->imagem);
                $curso->delete();
              
               
            }
         
            
            
            return $this->redirect('admin/cursos');
        }
        public function update($dado){
            if( ! isset($_SESSION['id'])){
                return $this->redirect('login');
            }

            $user = User::where('id',$_SESSION['id'])->first();

            if( ! $user->admin){
               
                return $this->redirect('login');
            }

            $curso = Curso::find($dado['curso']);
            $curso->nome =  $dado['nome'];
            $curso->pago =  $dado['pago'];

            if ($_FILES && $_FILES['imagem']['tmp_name']) {
                unlink('public/images/cursos/' . $curso->imagem);
                $extensao = pathinfo($_FILES['imagem']['name'],PATHINFO_EXTENSION);
                $pasta = 'public/images/cursos/';
                $temporario = $_FILES['imagem']['tmp_name'];
                $nome = str_replace(' ','-',$dado['nome']) . '-' . uniqid(). '.' . $extensao;

                move_uploaded_file($temporario,$pasta.$nome);
                    
      
                $curso->imagem =  $nome;
            }
            
            $curso->save();
              
               
            
         
            
            
            return $this->redirect('admin/cursos');
        }
    }