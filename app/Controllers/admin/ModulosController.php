<?php

    namespace App\Controllers\admin;

    use App\Core\Arquivos;

    use App\Models\Curso;
use App\Models\Modulo;
use App\Models\User;


class ModulosController extends Arquivos
    {
        public function index(){
            if( ! isset($_SESSION['id'])){
                return $this->redirect('login');
            }

            $user = User::where('id',$_SESSION['id'])->first();

            if( ! $user->admin){
               
                return $this->redirect('login');
            }
          

            $getArquivos = new  Arquivos;


            $modulos = Modulo::all();
            $cursos = Curso::all();
            
            return $this->view('admin.modulos.index',compact('modulos','getArquivos','cursos'));
        }
        public function show(){
            if( ! isset($_SESSION['id'])){
                return $this->redirect('login');
            }

            $user = User::where('id',$_SESSION['id'])->first();

            if( ! $user->admin){
               
                return $this->redirect('login');
            }

            $cursos = Curso::all();
            
            $route = $this->route('admin/modulos/store');
            
            
            return $this->view('admin.modulos.show',compact('route','cursos'));
        }
        public function store($data){
            if( ! isset($_SESSION['id'])){
                return $this->redirect('login');
            }

            $user = User::where('id',$_SESSION['id'])->first();

            if( ! $user->admin){
               
                return $this->redirect('login');
            }


            $modulo = new Modulo();

        
 
            $modulo->nome = $data['nome'];
            $modulo->curso_id = $data['curso_id'];
            $modulo->save();
            
            
            
            return $this->redirect('admin/modulos');
        }
        public function edit($data){
            if( ! isset($_SESSION['id'])){
                return $this->redirect('login');
            }

            $user = User::where('id',$_SESSION['id'])->first();

            if( ! $user->admin){
               
                return $this->redirect('login');
            }


            $modulo = Modulo::find($data['modulo']);

            $route = $this->route('admin/modulos/update/' . $modulo->id);
           
            $getArquivos = new  Arquivos;

            $cursos = Curso::all();
            
            return $this->view('admin.modulos.show',compact('modulo','getArquivos','route','cursos'));
        }
        public function delete($dado){
            if( ! isset($_SESSION['id'])){
                return $this->redirect('login');
            }

            $user = User::where('id',$_SESSION['id'])->first();

            if( ! $user->admin){
               
                return $this->redirect('login');
            }
            if ($modulo = Modulo::find($dado['modulo'])) {

                $modulo->delete();
              
               
            }
         
            
            
            return $this->redirect('admin/modulos');
        }
        public function update($dado){
            if( ! isset($_SESSION['id'])){
                return $this->redirect('login');
            }

            $user = User::where('id',$_SESSION['id'])->first();

            if( ! $user->admin){
               
                return $this->redirect('login');
            }

            $modulo = Modulo::find($dado['modulo']);
            $modulo->nome =  $dado['nome'];
            $modulo->curso_id =  $dado['curso_id'];
            $modulo->save();
              
               
            
         
            
            
            return $this->redirect('admin/modulos');
        }
    }