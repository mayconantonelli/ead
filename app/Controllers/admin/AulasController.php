<?php

    namespace App\Controllers\admin;

    use App\Core\Arquivos;
use App\Models\Aula;
use App\Models\Curso;
    use App\Models\Modulo;
    use App\Models\User;


    class AulasController extends Arquivos
    {
        public function index(){
            if( ! isset($_SESSION['id'])){
                return $this->redirect('login');
            }

            $user = User::where('id',$_SESSION['id'])->first();

            if( ! $user->admin){
               
                return $this->redirect('login');
            }
          

            $getArquivos = new  Arquivos;


            $aulas = Aula::all();
            $cursos = Curso::all();
            $modulos = Modulo::all();
            
            return $this->view('admin.aulas.index',compact('aulas','getArquivos','cursos','modulos'));
        }
        public function show(){
            if( ! isset($_SESSION['id'])){
                return $this->redirect('login');
            }

            $user = User::where('id',$_SESSION['id'])->first();

            if( ! $user->admin){
               
                return $this->redirect('login');
            }

            $cursos = Curso::all();
            $modulos = Modulo::all();
            
            $route = $this->route('admin/aulas/store');
            
            
            return $this->view('admin.aulas.show',compact('route','cursos','modulos'));
        }
        public function store($data){
            if( ! isset($_SESSION['id'])){
                return $this->redirect('login');
            }

            $user = User::where('id',$_SESSION['id'])->first();

            if( ! $user->admin){
               
                return $this->redirect('login');
            }


            $aula = new Aula();

        
 
            $aula->nome = $data['nome'];
            $aula->curso_id = $data['curso_id'];
            $aula->modulo_id = $data['modulo_id'];
            $aula->save();
            
            
            
            return $this->redirect('admin/aulas');
        }
        public function edit($data){
            if( ! isset($_SESSION['id'])){
                return $this->redirect('login');
            }

            $user = User::where('id',$_SESSION['id'])->first();

            if( ! $user->admin){
               
                return $this->redirect('login');
            }


            $aula = Aula::find($data['aula']);

            $route = $this->route('admin/aulas/update/' . $aula->id);
           
            $getArquivos = new  Arquivos;
            $modulos = Modulo::all();
            $cursos = Curso::all();
            
            return $this->view('admin.aulas.show',compact('modulos','getArquivos','route','cursos','aula'));
        }
        public function delete($dado){
            if( ! isset($_SESSION['id'])){
                return $this->redirect('login');
            }

            $user = User::where('id',$_SESSION['id'])->first();

            if( ! $user->admin){
               
                return $this->redirect('login');
            }
            if ($aula = Aula::find($dado['aula'])) {

                $aula->delete();
              
               
            }
         
            
            
            return $this->redirect('admin/aulas');
        }
        public function update($dado){
            if( ! isset($_SESSION['id'])){
                return $this->redirect('login');
            }

            $user = User::where('id',$_SESSION['id'])->first();

            if( ! $user->admin){
               
                return $this->redirect('login');
            }

            $aula = Aula::find($dado['aula']);
            $aula->nome =  $dado['nome'];
            $aula->curso_id =  $dado['curso_id'];
            $aula->modulo_id =  $dado['modulo_id'];
            $aula->save();
              
               
            
         
            
            
            return $this->redirect('admin/aulas');
        }
    }