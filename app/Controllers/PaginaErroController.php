<?php

namespace App\Controllers;

    use App\Core\Arquivos;


    class PaginaErroController extends Arquivos
    {
        public function index(){
            return $this->view('site.404');
        }

    }