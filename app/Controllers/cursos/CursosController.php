<?php

    namespace App\Controllers\cursos;

    use App\Core\Arquivos;
    use App\Core\Certificado;
    use App\Models\Aula;
    use App\Models\AulaAssistida;
    use App\Models\Curso;
    use App\Models\Modulo;
    use App\Models\User;
    use App\Models\UserCurso;

  

    class CursosController extends Arquivos
    {
        public function index(){

            if( ! isset($_SESSION['id'])){
                return $this->redirect('login');
            }


            $user = User::with('cursos')->where('id',$_SESSION['id'])->first();

            $cursosGratuitos = Curso::where('pago',0)->get();


           
            return $this->view('site.cursos',compact('user','cursosGratuitos'));
        }

        public function show($data)
        {
            if( ! isset($_SESSION['id'])){
                return $this->redirect('login');
            }

            $user_id = $_SESSION['id'];

            $curso = Curso::where('nome',str_replace('-',' ',$data["curso"]))->first();

            
            if($curso->pago == 1){
                $userCurso = UserCurso::where('user_id',$user_id)->where('curso_id',$curso->id)->first();

                if( ! $userCurso){
                    return $this->redirect('cursos');
                }
            }

            $modulos = Modulo::with('aulas')
                ->where('curso_id',$curso->id)
                ->get();



            $assistidos = AulaAssistida::where('curso_id', $curso->id)->where('user_id',$user_id)->get();





            $aulaUm = Aula::where('curso_id',$curso->id)->where('ordem',1)->first();
            


                
            return $this->view('site.curso-show',compact('modulos','curso','assistidos','aulaUm'));
        }

        public function showAulaAjax($data)
        {            
            if( ! isset($_SESSION['id'])){
                return;
            }
            $aulaAtual = Aula::where('id',$data['id'])->first();
            echo json_encode($aulaAtual);
        }

        public function showAulaVisualizadaAjax($data)
        {            
            if( ! isset($_SESSION['id'])){
                return;
            }

           
            $user_id = (int)$_SESSION['id'];
            $aula_id = (int)$data['aula_id'];
            $assistido = (int)$data['assistido'];
            $curso_id = (int)$data['curso_id'];
            


           
            $aulaAssistida = AulaAssistida::updateOrCreate(
                ['user_id' => $user_id, 'aula_id' => $aula_id,'curso_id'=>$curso_id],


                ['assistido' => $assistido]
            );
            


            echo json_encode($aulaAssistida);
        }
        
    }