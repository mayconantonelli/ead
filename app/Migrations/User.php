<?php
    namespace App\Migrations;

    use Illuminate\Database\Capsule\Manager;

    class User
    {
        public function up(){
            Manager::schema()
                ->create('users',function($table){
                    $table->increments('id');
                    $table->string('nome')->nullable();
                    $table->timestamps();
                });
        }
    }