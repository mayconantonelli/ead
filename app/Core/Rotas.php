<?php
    namespace App\Core;


    use Route\Web;

    class Rotas extends Arquivos
    {

        public static function getRotas($rota = null){
            $web = new Web;

            $todasRotas = $web->setRotas(); 
            
            $controller = new Controller;
            if( ! isset($todasRotas[$rota])){
                $controller->index($todasRotas['404']);
                return;
            }
                   
            $controller->index($todasRotas[$rota]);

        }


    }