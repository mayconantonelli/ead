<?php

    namespace App\Core;

    use Jenssegers\Blade\Blade;

    class Arquivos
    {
        public function view($caminho,$dados = array()){
            
            $blade = new Blade('app/Views', 'app/Views/cache');
          
            echo $blade->render($caminho, $dados);
        }
        
        public function view1($view, $dados = null){
            
            $arquivo =  '../app/Views/' . str_replace('.','/',$view) . '.php';

            extract((array)$dados);

            if(file_exists($arquivo)){
                include $arquivo;

                return;
            }

            echo 'view não existe!';
        }

        public function access($caminho){
            
            $caminhoComPastaPublic = 'public/' . $caminho;
            
         
                
                echo  'http://'. $_SERVER['SERVER_NAME'] . '/' .  $caminhoComPastaPublic;
    

          //  echo 'nao foi encontrado nada nesse caminho';
        }

        public function accessImg($caminho){
            
            $caminhoComPastaPublic = 'public/images/' . $caminho;
            
            if(file_exists($caminhoComPastaPublic)){
                
                echo  'http://'. $_SERVER['SERVER_NAME'] . '/' .  $caminhoComPastaPublic;
                return;
            }

            echo 'nao foi encontrado nada nesse caminho';
        }
        public function redirect($rota){
            
                
            $rota =   'http://'. $_SERVER['SERVER_NAME'] . '/' .  $rota;


            echo '<script>window.location.href = "' . $rota . '"</script>';
        }
        public function link($rota){
            
                
            echo   'http://'. $_SERVER['SERVER_NAME'] . '/' .  $rota;


        }
        public function route($rota){
            
                
            return   'http://'. $_SERVER['SERVER_NAME'] . '/' .  $rota;


        }
    }