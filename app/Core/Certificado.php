<?php

    namespace App\Core;

use App\core\AlphaPdf;
use App\Models\Curso;
use App\Models\User;

class Certificado
    {
        private $email;
        private $user_nome;
        private $cpf;
        private $empresa;
        private $curso;
        private $cargaHoraria;

        function __construct()
        {
            
            $this->empresa  = "Universo da Programação";
            $this->email    = 'maykym.007@hotmail.com';
            $this->user_nome     = utf8_decode(ucwords('wesley gay'));
            $this->cpf      = '106.392.039.65';
            $this->curso    = "Curso php completo";
            $this->cargaHoraria  = "8 horas";
        }

        public function gerar(){

           
            $texto2 = utf8_decode("pela participação no " . $this->curso . "com carga horária total de " . $this->cargaHoraria .".");
            $texto3 = utf8_decode("Maringá, ".utf8_encode(strftime( '%d de %B de %Y', strtotime( date( 'Y-m-d' ) ) )));


            $pdf = new AlphaPdf();

            // Orientação Landing Page ///
            $pdf->AddPage('L');

            $pdf->SetLineWidth(1.5);


            // desenha a imagem do certificado
            $pdf->Image('public/images/certificado.jpg',0,0,295);

            // opacidade total
            $pdf->SetAlpha(1);

            // Mostrar texto no topo
            $pdf->SetFont('Arial', '', 15); // Tipo de fonte e tamanho
            $pdf->SetXY(109,46); //Parte chata onde tem que ficar ajustando a posição X e Y
            $pdf->MultiCell(265, 10, utf8_decode($this->empresa), '', 'L', 0); // Tamanho width e height e posição

            // Mostrar o nome
            $pdf->SetFont('Arial', '', 30); // Tipo de fonte e tamanho
            $pdf->SetXY(20,86); //Parte chata onde tem que ficar ajustando a posição X e Y
            $pdf->MultiCell(265, 10, $this->user_nome , '', 'C', 0); // Tamanho width e height e posição

            // Mostrar o corpo
            $pdf->SetFont('Arial', '', 15); // Tipo de fonte e tamanho
            $pdf->SetXY(20,110); //Parte chata onde tem que ficar ajustando a posição X e Y
            $pdf->MultiCell(265, 10, $texto2, '', 'C', 0); // Tamanho width e height e posição

            // Mostrar a data no final
            $pdf->SetFont('Arial', '', 15); // Tipo de fonte e tamanho
            $pdf->SetXY(32,172); //Parte chata onde tem que ficar ajustando a posição X e Y
            $pdf->MultiCell(165, 10, $texto3, '', 'L', 0); // Tamanho width e height e posição

            $pdfdoc = $pdf->Output('', 'S');

            $certificado="public/pdf/$this->cpf.pdf"; //atribui a variável $certificado com o caminho e o nome do arquivo que será salvo (vai usar o CPF digitado pelo usuário como nome de arquivo)
            $pdf->Output($certificado,'F'); //Salva o certificado no servidor (verifique se a pasta "arquivos" tem a permissão necessária)
            // Utilizando esse script provavelmente o certificado ficara salvo em www.seusite.com.br/gerar_certificado/arquivos/999.999.999-99.pdf (o 999 representa o CPF digitado pelo usuário)

            $pdf->Output(); // Mostrar o certificado na tela do navegador


            
        }

    }