<?php

    namespace App\Core;

    class Controller
    {
        public  function index($controllerMetodo){
 
            $controller = explode('@',$controllerMetodo)[0];
            $metodo = explode('@',$controllerMetodo)[1];         
        
            
            if( ! class_exists($controller = 'App\\Controllers\\' . $controller)){
                throw new \Exception('controller não existe');

            }

            $controller = new $controller();

            if( ! method_exists($controller,$metodo)){
                throw new \Exception('methodo não existe');
                
            }

            $controller->$metodo();
            
        
           
        }




    }