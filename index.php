<?php



    session_start();
    setlocale( LC_ALL, 'pt_BR', 'pt_BR.iso-8859-1', 'pt_BR.utf-8', 'portuguese' );
    date_default_timezone_set( 'America/Sao_Paulo' );
    require_once 'vendor/autoload.php';
    require_once 'config.php';
    require_once 'bootstrap.php';
    require_once  'route/Web.php';

