<?php

    namespace Src\DB;

use Exception;
use PDO;

class DataBase
    {
        private static $pdo;
        protected $objeto;
        

		private  function conectar(){
			if(self::$pdo == null){ 
				try{
					self::$pdo = new PDO(
                        'mysql:host='. 'localhost' . 
                        ';dbname='   . 'test',
                        'root',
                        '', 
                        array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
                    ));
					self::$pdo->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);	
				}catch(Exception $e){
					exit($e->getMessage());
				}	
			}
			return self::$pdo;
        }
        public function executeSql($quey,array $params = NULL){
            $this->objeto = $this->conectar()->prepare($quey);
            if($params){
                foreach($params as $key => $value){
                    $this->objeto->bindvalue($key,$value);
                }

            }
            return $this->objeto->execute();
        }

        public  function listarDados($quey, $params = NULL){
            $this->objeto = $this->conectar()->prepare($quey);
            
            if($params){
                foreach($params as $key => $value){
                    $this->objeto->bindvalue($key,$value);
                }

            }
            
            $this->objeto->execute();

            return json_decode(json_encode($this->objeto->fetchAll()));
        }

        public  function listarDado($quey){
            $this->objeto = $this->conectar()->prepare($quey);

            $this->objeto->execute();

            return json_decode(json_encode($this->objeto->fetch()));
        }

        public  function totalDados(){
          
            return $this->objeto->rowCount();
        }
    }





    