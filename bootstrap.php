<?php

    use Illuminate\Database\Capsule\Manager;

    $manager = new Manager();
   

    $config = [
        "driver"=>"mysql",
        "host"=>"localhost",
        "database"=>BANCO_NOME,
        "username"=>BANCO_USUARIO,
        "password"=>BANCO_SENHA,
    ];


    $manager->addConnection($config);

    $manager->setAsGlobal();

    $manager->bootEloquent();
   
